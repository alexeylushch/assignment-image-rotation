#ifndef FILE_PRINT_STATUS_H
#define FILE_PRINT_STATUS_H

#include <inttypes.h>
#include <stdbool.h>
#include "bmp.h"
#include "file.h"

bool check_file_status(enum file_status status_code);

#endif

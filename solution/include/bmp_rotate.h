#ifndef BMP_ROTATE
#define BMP_ROTATE

#include "image.h"

struct image bmp_rotate(struct image image);

#endif

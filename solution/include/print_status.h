#ifndef PRINT_STATUS_H
#define PRINT_STATUS_H

#include <inttypes.h>
#include "bmp.h"
#include "file.h"

typedef int8_t (*print_status)(void);
void print_status_error(const char* string);
#endif

#ifndef BMP_PRINT_STATUS_H
#define BMP_PRINT_STATUS_H

#include <inttypes.h>
#include <stdbool.h>
#include "bmp.h"
#include "print_status.h"


bool check_bmp_read_status(enum read_status status_code);
bool check_bmp_write_status(enum write_status status_code);

#endif

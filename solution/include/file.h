#ifndef FILE_H
#define FILE_H

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>


enum file_status  {
  FILE_RW_LIB_FILE_OK = 0,
  FILE_RW_LIB_FAILED_OPEN,
};

FILE* open_read_file(const char* file_name, bool is_binary);
FILE* open_write_file(const char* file_name, bool is_binary);
enum file_status get_file_status(FILE* file);

#endif

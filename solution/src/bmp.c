#include "bmp.h"
#include "image.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>



#define BMP_CODE 0x4d42
#define BMP_BITS 24

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


static uint32_t compare_padding(uint32_t width) {
    uint32_t padding = width % 4;
    return padding;
}


static bool is_bmp(int code) {
    if (code == BMP_CODE) {
        return true;
    }
    return false;
}


static struct bmp_header create_bmp_header(struct image const* img, uint32_t bit_count) {


    const struct bmp_header bmp =  {
		.bfType = BMP_CODE,
		.bOffBits = 54,
        .bfReserved = 0,
		.biSize = 40,
		.biPlanes = 1,
		.biBitCount = bit_count,
		.biCompression = 0,
		.biWidth = img->width,
		.biHeight = img->height
	};

    return bmp;

}

static struct image initial_image(uint32_t height, uint32_t width) {
    struct image img = {.height = height, .width = width, .data=malloc(sizeof(struct pixel) * height * width)};
    return img;
}

enum read_status from_bmp(FILE* in, struct image* img) {

    struct bmp_header bmp = {0};


    if (!fread(&bmp, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_SIGNATURE;
    }

    if (!is_bmp(bmp.bfType)) {
        return READ_INVALID_HEADER;
    }


    if (bmp.biBitCount != BMP_BITS) {
        return READ_INVALID_BITS;
    }


    fseek(in, bmp.bOffBits, SEEK_SET);
    struct image img_init = initial_image(bmp.biHeight, bmp.biWidth);

    const uint32_t padding_size = compare_padding(bmp.biWidth);    


    for (uint64_t i = 0; i < img_init.height; i++) {
        fread(img_init.data + i * bmp.biWidth, sizeof(struct pixel), bmp.biWidth, in);
        if (fseek(in, padding_size, SEEK_CUR) != 0) {
            return READ_INVALID_BITS;
        };
    }

    *img = img_init;
    return READ_OK;
	
}

enum write_status to_bmp( FILE* out, struct image const* img ) {


	const struct bmp_header bmp = create_bmp_header(img, BMP_BITS);


    fwrite(&bmp, sizeof(struct bmp_header), 1, out);
    fseek(out, bmp.bOffBits, SEEK_SET);
	const uint32_t padding_elements[3] = {0};
    const uint32_t padding_size = compare_padding(bmp.biWidth);    

    for (uint64_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        fwrite(padding_elements, 1, padding_size, out);
    }

    return WRITE_OK;
}




#undef BMP_CODE
#undef BMP_BITS

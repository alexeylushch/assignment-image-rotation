#include "bmp_rotate.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h> 

static uint64_t get_pixel_new_image(struct image image, uint64_t i, uint64_t j) {
    return j * image.height + i;
}

static uint64_t get_pixel_image(struct image image, uint64_t i, uint64_t j) {
    return (image.height - i - 1) * image.width + j;
}

static void set_pixel_new_image(struct image image, struct image new_image, uint64_t pixel, uint64_t new_pixel) {
    new_image.data[new_pixel] = image.data[pixel];
}

static void apply_new_pixel(struct image image, struct image new_image, uint64_t i, uint64_t j) {
    uint64_t pixel_new = get_pixel_new_image(image, i, j);
    uint64_t pixel = get_pixel_image(image, i, j);
    set_pixel_new_image(image, new_image, pixel, pixel_new);
}
struct image bmp_rotate(struct image image) {

    struct image new = {
        .width = image.height,
        .height = image.width,
        .data = malloc(sizeof(struct pixel) * image.height * image.width)
    };

    for (uint64_t i = 0; i < image.height; i++) {
        for (uint64_t j = 0; j < image.width; j++) {
            apply_new_pixel(image, new, i, j);
        }
    }

	return new;

}

// struct image bmp_rotate(struct image image) {
//     struct image new = {
//         .width = image.height,
//         .height = image.width,

//     };

//     new.data = malloc(sizeof(struct pixel) * new.height * new.width);

//     for (size_t i = 0; i < image.height; i++) {
//         for (size_t j = 0; j < image.width; j++) {

//             size_t new_index = j * image.height + i;
//             size_t index = (image.height - i - 1) * image.width + j;

//             new.data[new_index] = image.data[index];
//         }
//     }

		

// 	return new;

// }


#include "file.h"
#include "bmp.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"

#include "bmp_print_status.h"
#include "bmp_rotate.h"
#include "file_print_status.h"



int main(int argc, char** argv) {
	
	if (argc < 3) {
		fprintf(stderr, "%s", "Please, input file name and output file name...");
		return 0;
	}

	

	FILE* file;
	struct image image;

	file = open_read_file(argv[1], true);
	enum file_status file_open_status = get_file_status(file);
	if (check_file_status(file_open_status) == false) {
		fclose(file);
		return 0;
	}


	enum read_status read_file_code = from_bmp(file, &image);


	
	if (check_bmp_read_status(read_file_code) == false) {
		fclose(file);
		return 0;
	}

	fclose(file);


	struct image new_image = bmp_rotate(image);
    FILE* new = open_write_file(argv[2], true);
	enum write_status write_file_code = to_bmp(new, &new_image);
	free(new_image.data);




	if (check_bmp_write_status(write_file_code) == false) {
		fclose(new);
		return 0;
	}


	fclose(new);

	free(image.data);


	

	return 0;

}

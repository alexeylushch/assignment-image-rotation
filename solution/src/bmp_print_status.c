#include "print_status.h"
#include "bmp.h"
#include "bmp_print_status.h"
#include <inttypes.h>
#include <stdio.h>


/*---------Read BMP---------*/
static int8_t bmp_read_ok(void) {
	return 1;
}

static int8_t bmp_read_invalid_signatue(void) {
	print_status_error("Invalid signature...");
	return 0;
}

static int8_t bmp_read_invalid_bits(void) {
	print_status_error("Invalid bits...");
	return 0;
}

static int8_t bmp_read_invalid_header(void) {
	print_status_error("Invalid header...");
	return 0;
}

/*---------Write BMP---------*/

static int8_t bmp_write_ok(void) {
	return 1;
}

static int8_t bmp_write_error(void) {
	print_status_error("Error while writing file...");
	return 0;
}

static const print_status bmp_read_statuses[] = {
	[READ_OK] = &bmp_read_ok,
	[READ_INVALID_SIGNATURE] = &bmp_read_invalid_signatue, 
	[READ_INVALID_BITS] = &bmp_read_invalid_bits, 
	[READ_INVALID_HEADER] = &bmp_read_invalid_header
};

static const print_status bmp_write_statuses[] = {
	[WRITE_OK] = &bmp_write_ok,
	[WRITE_ERROR] = &bmp_write_error
};

bool check_bmp_read_status(enum read_status status_code) {

	if (bmp_read_statuses[status_code]() == 1) {
		return true;
	}
    return false;
}

bool check_bmp_write_status(enum write_status status_code) {

	if (bmp_write_statuses[status_code]() == 1) {
		return true;
	}
    return false;
}


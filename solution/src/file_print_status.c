#include "file_print_status.h"
#include "print_status.h"
#include <inttypes.h>
#include <stdio.h>



#define COUNT_OF_FILE_STATUSES 2



/*---------File---------*/

static int8_t file_ok(void) {
	return 1;
}

static int8_t file_failed_open(void) {
	print_status_error("Failed to open file...");
	return 0;
}



static const print_status file_statuses[COUNT_OF_FILE_STATUSES] = {
	&file_ok,
	&file_failed_open
};



bool check_file_status(enum file_status status_code) {
    if (file_statuses[status_code]() == 1) {
		return true;
	}
	return false;
}




#undef COUNT_OF_FILE_STATUSES

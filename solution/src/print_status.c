#include "print_status.h"
#include <stdio.h>

void print_status_error(const char* string) {
    fprintf(stderr, "%s\n", string);
}

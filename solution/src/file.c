#include "file.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>



FILE* open_write_file(const char* file_name, bool is_binary) {

    const char* mode = "w";
    if (is_binary) {
        mode = "wb";
    }

    return fopen(file_name, mode);
}

FILE* open_read_file( const char* file_name, bool is_binary) {

    const char* mode = "r";
    if (is_binary) {
        mode = "rb";
    }

    return fopen(file_name, mode);
}

enum file_status get_file_status(FILE* file) {

    if (file == NULL) {
        return FILE_RW_LIB_FAILED_OPEN;
    }

    return FILE_RW_LIB_FILE_OK;
}
